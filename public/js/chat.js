'use strict';
/* global io $ document window */

/**
 * @param {string} app_id the application identifier send to the server
 */
const initSocket = (app_id) => {
	console.log('using hostname: '+window.location.origin);
	const socket = io(window.location.origin);
	let sessionid;
	// register room to the server
	socket.on('connect', () => {
		console.log('socket.io connected!');
		sessionid = socket.io.engine.id;
		socket.emit('app_id', app_id);
	});

	// on response
	socket.on('message', (data) => {
		let sender = data.socketid;
		if(data.socketid === sessionid) {
			sender = 'you';
		}
		console.log('message from '+sender+': '+JSON.stringify(data));

		$('#chatMessages').append(data.client_name+' says "'+data.str+'"</br>');
	});

	// sending messages
	$('#messageInput').bind('keypress', (e) => {
		const code = (e.keyCode ? e.keyCode : e.which);
		if (code == 13 && socket) { // keyboard Enter
			const msg = {};
			msg.client_name = $('#usernameInput').val();
			msg.app_id = app_id;
			msg.time = Date.now();
			msg.str = $('#messageInput').val();
			socket.json.emit('message', msg);
			$('#messageInput').val('');
        }
	});
};

const getUrlParameter = (sParam) => {
    const sPageURL = decodeURIComponent(window.location.search.substring(1));
    const sURLVariables = sPageURL.split('&');

    for (let i = 0; i < sURLVariables.length; i++) {
        const sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

const initRoomNameCheck = () => {
	$('#roomNameInput').on('change paste keyup', (event) => {
		if($(event.currentTarget).val()) {
			$('#roomForm button').prop('disabled', false);
		} else {
			$('#roomForm button').prop('disabled', true);
		}
	});
};

const initUsernameCheck = () => {
	$('#usernameInput').on('change paste keyup', function() {
		if($('#usernameInput').val()) {
			$('#messageInput').prop('disabled', false);
		} else {
			$('#messageInput').prop('disabled', true);
		}
	});
}

$(document).ready(() => {
    const room = getUrlParameter('room');
    if(room) {
        $('#roomForm').hide();
        $('#roomName').text('Room name: ' + room);
        $('#shareRoom').append(`<a href="${window.location.href}" target="_blank">${window.location.href}</a>`)
        initUsernameCheck();
        $('#chat').show();
        initSocket(room);
    } else {
        initRoomNameCheck();
    }
});

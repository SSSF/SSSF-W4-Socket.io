# Assignment: W4-3: Socket.io

[Link to the server](https://gitlab.com/SSSF/SSSF-W4-Socket.io/blob/master/server.js)

[Link to the client-side Javascript](https://gitlab.com/SSSF/SSSF-W4-Socket.io/blob/master/public/js/chat.js)

## Namespaces VS Rooms

### Namespaces
Socket.IO allows you to “namespace” your sockets, which essentially means assigning different endpoints or paths.

This is a useful feature to minimize the number of resources (TCP connections) and at the same time separate concerns within your application by introducing separation between communication channels.

**Example of custom namespace**

Server-side:
```javascript
var nsp = io.of('/my-namespace');
nsp.on('connection', function(socket){
  console.log('someone connected');
});
nsp.emit('hi', 'everyone!');
```

Client-side:
```javascript
var socket = io('/my-namespace');
```

### Rooms
Within each namespace, you can also define arbitrary channels that sockets can join and leave.
```javascript
io.on('connection', function(socket){
  socket.join('some room');
});
```
And then simply use to or in (they are the same) when broadcasting or emitting:
```javascript
io.to('some room').emit('some event');
```
To leave a channel you call leave in the same fashion as join.

## How could I utilize websockets in my project
I thought about using it to keep our shared shopping list updated. Every time someone add a new item in a list, everyone in the group will receive the new item. Without websockets, the user would have to refresh it manually to obtain the last items added by his friends.